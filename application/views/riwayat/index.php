<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Riwayat Konsultasi</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Riwayat Konsultasi
        </div>
        <div class="card-body">
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th></th>
                <th>Tanggal Konsultasi</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach ($riwayat as $row) : ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= $row->tanggal ?></td>
                  <td>
                  	<a href="<?php echo site_url('riwayat/laporan/' . $row->id); ?>" class="btn btn-primary"><i class="fa fa-file"></i> Cetak Laporan</a>
                  	<a href="<?php echo site_url('riwayat/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Riwayat Konsultasi ini ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data">Hapus</a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</main>
</div>