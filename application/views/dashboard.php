<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <?php if ($this->session->userdata['id_grup_user'] != 3 && $this->session->userdata['id_grup_user'] != 4 && $this->session->userdata['id_grup_user'] != 5) { ?>
      <li class="breadcrumb-item">
        <a href="<?= base_url('dashboard'); ?>">Admin</a>
      </li>
    <?php } ?>
    <li class="breadcrumb-item active">Dashboard</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="row">
        <div class="col-sm-12 col-lg-12">
          <div class="card-body">
            <div class="alert alert-primary" role="alert"><i class="nav-icon icon-speedometer"></i> Dashboard</div>
            <?php //if ($this->session->userdata['id_grup_user'] == '1') {
            $this->db->where("id", $this->session->userdata['id_grup_user']);
            $cek = $this->db->get('master_grup_user');

            if ($cek->num_rows() > 0) {
              foreach ($cek->result() as $ck) {
                $nama_grup = $ck->nama_grup;
              }
            }

            ?>

            <div class="alert alert-primary" role="alert">Selamat Datang di Sistem Pakar Diagnosa Awal Penyakit Alergi pada Anak, Anda login sebagai <?= $nama_grup ?></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>