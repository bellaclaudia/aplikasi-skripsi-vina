<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Master Diagnosa</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Master Diagnosa
        </div>
        <div class="card-body">
          <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Data</button><br><br>
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th></th>
                <th>Alergi</th>
                <th>Gejala</th>
                <th>Bobot Kepercayaan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach ($data_diagnosa as $row) : ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= $row->nama_alergi ?></td>
                  <td><?= $row->nama_gejala ?></td>
                  <td><?= $row->bobot ?></td>
                  <td>
                    <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo site_url('diagnosa/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Diagnosa dengan ID <?= $row->id; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAdd', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Jenis Alergi</label>
	              	<div class="col-md-9">
		                <select class="form-control select2-single" id="select2-1" name="id_alergi">
		                  <option value="0" selected disabled>Pilih Data Jenis Alergi</option>
		                  <?php foreach ($alergi as $k) : ?>
		                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
		                  <?php endforeach; ?>
		                </select>
		                <small class="text-danger">
		                  <?php echo form_error('id_alergi') ?>
		                </small>
	              	</div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Gejala</label>
	              	<div class="col-md-9">
		                <select class="form-control select2-single" id="select2-2" name="id_gejala">
		                  <option value="0" selected disabled>Pilih Data Gejala</option>
		                  <?php foreach ($gejala as $k) : ?>
		                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
		                  <?php endforeach; ?>
		                </select>
		                <small class="text-danger">
		                  <?php echo form_error('id_gejala') ?>
		                </small>
	              	</div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Bobot Kepercayaan</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" placeholder="Isi Bobot Kepercayaan" name="bobot" value="<?= set_value('bobot'); ?>" required>
                      <small class="text-danger">
                        <?php echo form_error('bobot') ?>
                      </small>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_diagnosa as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="<?php echo site_url('diagnosa/edit'); ?>" method="post">
                      <div class="form-group row">
	                    <label class="col-md-3 col-form-label">Jenis Alergi</label>
		              	<div class="col-md-9">
		              		<input class="form-control" type="hidden" name="id" value="<?= $row->id; ?>">
			                <select class="form-control select2-single" id="select2-3" name="id_alergi">
			                  <option value="0" disabled>Pilih Data Jenis Alergi</option>
			                  <?php foreach ($alergi as $k) : ?>
			                    <option value=" <?= $k['id']; ?>" <?php if ($k['id'] == $row->id_alergi) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
			                  <?php endforeach; ?>
			                </select>
			                <small class="text-danger">
			                  <?php echo form_error('id_alergi') ?>
			                </small>
		              	</div>
	                  </div>
	                  <div class="form-group row">
	                    <label class="col-md-3 col-form-label">Gejala</label>
		              	<div class="col-md-9">
			                <select class="form-control select2-single" id="select2-4" name="id_gejala">
			                  <option value="0" disabled>Pilih Data Gejala</option>
			                  <?php foreach ($gejala as $k) : ?>
			                    <option value=" <?= $k['id']; ?>" <?php if ($k['id'] == $row->id_gejala) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
			                  <?php endforeach; ?>
			                </select>
			                <small class="text-danger">
			                  <?php echo form_error('id_gejala') ?>
			                </small>
		              	</div>
	                  </div>
	                  <div class="form-group row">
	                    <label class="col-md-3 col-form-label">Bobot Kepercayaan</label>
	                    <div class="col-md-9">
	                      <input class="form-control" type="text" placeholder="Isi Bobot Kepercayaan" name="bobot" value="<?= $row->bobot; ?>" required>
	                      <small class="text-danger">
	                        <?php echo form_error('bobot') ?>
	                      </small>
	                    </div>
	                  </div>
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>