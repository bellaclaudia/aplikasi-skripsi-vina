<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Pasien</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Pasien
        </div>
        <div class="card-body">
          <button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Tambah Data</button><br><br>
          <?php if ($this->session->flashdata('message')) :
            echo $this->session->flashdata('message');
          endif; ?>
          <table class="table table-striped table-bordered datatable">
            <thead>
              <tr>
                <th></th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>TTL</th>
                <th>Nama Ibu</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $i = 1; ?>
              <?php foreach ($data_pasien as $row) : ?>
                <tr>
                  <td><?= $i++; ?></td>
                  <td><?= $row->nama ?></td>
                  <td><?= $row->alamat ?></td>
                  <td><?= $row->tmp_lahir ?> / <?= $row->tgl_lahir ?></td>
                  <td><?= $row->nama_ibu ?></td>
                  <td>
                    <a data-toggle="modal" data-target="#modal-edit<?= $row->id; ?>" class="btn btn-success btn-circle" data-popup="tooltip" data-placement="top" title="Edit Data" data-popup="tooltip" data-placement="top"><i class="fa fa-edit"></i></a>
                    <a href="<?php echo site_url('pasien/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Pasien <?= $row->nama; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <div class="modal fade" id="tambahData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Tambah Data</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <?php
                  $attributes = array('id' => 'FrmAdd', 'method' => "post", "autocomplete" => "off", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                  echo form_open('', $attributes);
                  ?>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Nama Lengkap</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="nama" placeholder="Isi Nama Lengkap" required>
                      <small class="text-danger">
                        <?php echo form_error('nama') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Alamat</label>
                    <div class="col-md-9">
                      <textarea class="form-control" name="alamat" placeholder="Isi Alamat"><?= set_value('alamat'); ?></textarea>
                      <small class="text-danger">
                        <?php echo form_error('alamat') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Tempat Lahir</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" placeholder="Isi Tempat Lahir" name="tmp_lahir" value="<?= set_value('tmp_lahir'); ?>">
                      <small class="text-danger">
                        <?php echo form_error('tmp_lahir') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Tgl. Lahir</label>
                    <div class="col-md-9">
                      <input class="form-control" type="date" name="tgl_lahir" value="<?= set_value('tgl_lahir'); ?>">
                      <small class="text-danger">
                        <?php echo form_error('tgl_lahir') ?>
                      </small>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-md-3 col-form-label">Nama Ibu</label>
                    <div class="col-md-9">
                      <input class="form-control" type="text" name="nama_ibu" placeholder="Isi Nama Ibu" value="<?= set_value('nama_ibu'); ?>">
                      <small class="text-danger">
                        <?php echo form_error('nama_ibu') ?>
                      </small>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                    <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                  </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <?php $no = 0;
          foreach ($data_pasien as $row) : $no++; ?>
            <div class="modal fade" id="modal-edit<?= $row->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Data</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="<?php echo site_url('pasien/edit'); ?>" method="post">
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Nama Lengkap</label>
                        <div class="col-md-9">
                          <input class="form-control" type="hidden" name="id" value="<?= $row->id; ?>">
                          <input class="form-control" type="text" name="nama" placeholder="Isi Nama Lengkap" value="<?= $row->nama; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('nama') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Alamat</label>
                        <div class="col-md-9">
                          <textarea class="form-control" name="alamat" placeholder="Isi Alamat" required><?= $row->alamat; ?></textarea>
                          <small class="text-danger">
                            <?php echo form_error('alamat') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Tempat Lahir</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" placeholder="Isi Tempat Lahir" name="tmp_lahir" value="<?= $row->tmp_lahir; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('tmp_lahir') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Tgl. Lahir</label>
                        <div class="col-md-9">
                          <input class="form-control" type="date" name="tgl_lahir" value="<?= $row->tgl_lahir; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('tgl_lahir') ?>
                          </small>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Nama Ibu</label>
                        <div class="col-md-9">
                          <input class="form-control" type="text" name="nama_ibu" placeholder="Isi Nama Ibu" value="<?= $row->nama_ibu; ?>" required>
                          <small class="text-danger">
                            <?php echo form_error('nama_ibu') ?>
                          </small>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Batal</button>
                        <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</main>
</div>