<style type="text/css">
	h1{
		text-align: center;
	}
	.shadow{
    box-shadow: 0 1px 11px 0 rgba(0,0,0,0.18),0 1px 15px 0 rgba(0,0,0,0.15)
  }

    tr:nth-child(2n-7) {background-color: white;}
    tr:nth-child(2n-6) {background-color: rgb(230,230,230);}
    tr td:first-child{ border-top-left-radius: 10px;}
    tr td:last-child{ border-top-right-radius: 10px;}
</style>
<?php
if (!isset($_GET['id_gejala'])) {
redirect(site_url('pasien/pendaftaran?cek=pilih'));
}
$i=0;

foreach ($_GET['id_gejala'] as $q) {
	$nilaiya = 0;
	$ge[]=$q;
	foreach ($g as $we) {
	if ($q == $we->id) {
		$gj[]=$we->nama;
	}
	}
	$j=0;	

	foreach ($gejala as $w) {
		if ($w->id_gejala == $q) {
			foreach ($penyakit as $e) {
				$data = $this->db->query("SELECT count(*) as jml FROM diagnosa WHERE id_gejala = '$w->id_gejala'")->row_array();
				$jumlah = $data['jml'];
				if ($w->id_alergi == $e->id) {
					$var[$i][$j]= 'P'.$e->id;
					$nilai[$i]=$w->bobot;
					$nilaiya+=$w->bobot;
					$nilai1[$i]=round($nilaiya/$jumlah,2);
				}
			}
			$j++;
		}
	}
	$i++;
}
for ($i=0; $i < count($var); $i++) { 
	// var_dump($var); die();
	$evidence[$i][0]=implode(',', $var[$i]);
	$evidence[$i][1]=$nilai[$i];

}
$w=array();
foreach ($penyakit as $q) {
	$w[]='P'.$q->id;
}
$fod= implode(',', $w);
$densitas_baru=array();
while(!empty($evidence)){
	$densitas1[0]=array_shift($evidence);
	$densitas1[1]=array($fod,1-$densitas1[0][1]);
	$densitas2=array();
	if(empty($densitas_baru)){
		$densitas2[0]=array_shift($evidence);
	}else{
		foreach($densitas_baru as $k=>$r){
			if($k!="&theta;"){
				$densitas2[]=array($k,$r);
			}
		}
	}
	$theta=1;
	foreach($densitas2 as $d) $theta-=$d[1];
	$densitas2[]=array($fod,$theta);
	$m=count($densitas2);
	$densitas_baru=array();
	for($y=0;$y<$m;$y++){
		for($x=0;$x<2;$x++){
			if(!($y==$m-1 && $x==1)){
				$v=explode(',',$densitas1[$x][0]);
				$w=explode(',',$densitas2[$y][0]);
				sort($v);
				sort($w);
				$vw=array_intersect($v,$w);
				if(empty($vw)){
					$k="&theta;";
				}else{
					$k=implode(',',$vw);
				}
				if(!isset($densitas_baru[$k])){
					$densitas_baru[$k]=$densitas1[$x][1]*$densitas2[$y][1];
				}else{
					$densitas_baru[$k]+=$densitas1[$x][1]*$densitas2[$y][1];
				}
			}
		}
	}
	foreach($densitas_baru as $k=>$d){
		
		if($k!="&theta;"){
			if ((1-(isset($densitas_baru["&theta;"])?$densitas_baru["&theta;"]:0)) == 0) {
				redirect(site_url('pasien/pilih?cek=null'));
			}
			$densitas_baru[$k]=$d/(1-(isset($densitas_baru["&theta;"])?$densitas_baru["&theta;"]:0));
			

		}
	}
}

unset($densitas_baru["&theta;"]);
arsort($densitas_baru);
$codes=array_keys($densitas_baru);

$nilai=round($densitas_baru[$codes[0]]*100,2);
for ($i=1; $i <count($codes) ; $i++) { 
	foreach ($variable = explode(',', $codes[$i]) as $q) {
		$j=1;
	 	foreach ($variable = explode('P', $q) as $w) {
	 		
	 		foreach ($penyakit as $e) {
	 			if ($e->id == $w) {
	 				$nmn[$i][]=$e->nama;
	 			}
	 		}
	 		
	 		$j++;
	 	}
	 } 
}
$nma=explode(',', $codes[0]);
foreach ($nma as $q) {
	$nmac[]=explode('P',$q);
}
for ($i=0; $i <count($nmac) ; $i++) { 
foreach ($penyakit as $q) {
	if ($q->id == $nmac[$i][1]) {
		$nama=$q->nama;
		$idd=$q->id;
		$pen=$q->penanganan;
	}
}
}
foreach ($penyakit as $q) {
	if ($nama == $q->nama) {
		$idd=$q->id;
	
	foreach ($gejala as $w) {
		if ($q->id == $w->id_alergi) {
			foreach ($g as $e) {
				if ($w->id_gejala == $e->id) {
					$gjl[]=$e->nama;
				}
			}
		}
	}}
}

?>




<main class="main">
  <ol class="breadcrumb">
    <li class="breadcrumb-item active">Pendaftaran Pasien</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Hasil Diagnosa
          <a href="<?= base_url('pasien/pendaftaran'); ?>"><button style="margin-left: 96%" class="btn btn-sm btn-success mb-1" type="button" data-toggle="modal" data-target="#tambahData">Kembali</button></a>
        </div>
        <div class="card-body">
					
					<div class="col-3"></div>
						<div class="col-6 bg-light shadow" style="padding: 5% 5%;border-radius: 20px">
							<h1 style="letter-spacing: 5px;" class="">HASIL DIAGNOSA</h1>
					<hr style="background-color:black;margin-bottom: 50px ">
							<center><small>Nama Penyakit:</small>
							<h1 style="text-align: left;font-size: 50px;"><?php echo $nama?></h1></center>
							<hr>

					<small>Persentase:</small>
					<br>
					<h4 style="font-weight: bold;"><?php echo number_format($nilai,2,',','') ?>%</h4>
					<hr>
					<small style="">Cara Penanganan:</small>
					<hr>
					<?php 
					$i=1;
					foreach ($penyakit as $q) {
					if ($q->id ==$idd) {
					?>
					<div class="col-6" style="text-align: justify;">
						<p><b><?php echo $i?>).</b> <?php echo $q->penanganan?></p>
					</div>
					<?php 
				$i++;}}
					?>	
					</center>
						</div>
						<div class="col-12 row" style="margin-top: 5%">
							<div class="col-6 bg-light shadow" style="padding: 20px 50px;border-radius: 20px">
								<label>Gejala Yang di pilih:</label>
								
								<?php 
								foreach ($gj as $q) {
									echo "<h5>-".$q."</h5>";
								}
								?>
							</div>
							<div class="col-6 bg-light shadow" style="padding: 20px 50px;border-radius: 20px">
							
								<label>Gejala Penyakit:</label>
								<?php 
								foreach ($gjl as $q) {
									echo "<h5>-".$q."</h5>";
								}
								?>
							</div>
						</div>
						<div class="col-12">
							<hr>
						</div>


						
					<div class="col-12 row">
					<a href="laporan?pen=<?= $idd?>&persen=<?php echo $nilai ?>&nama=<?php echo $nama?>" class="simpan col-12" style=";text-align: center;color: "><i class="fa fa-file"></i> Cetak Hasil Konsultasi</a>
							
					</div>
	
<?php 
	if (count($codes) !=1) {
?>
<div class="col-12  bg-light shadow" style="margin-top: 5%;padding: 20px 20px;border-radius: 20px;">
	<h3 style="text-align: center;">HASIL PERSENTASI PENYAKIT LAINYA</h3>
	<hr>
	<table class="table table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama Penyakit</th>
				<th>Persentasi</th>
			</tr>
		</thead>
		<tbody>
			<?php 
		$i=1;
		foreach ($nmn as $key => $value) {
			?>
			<tr>
				<td><?= $i?></td>
				<td><p>
					<?php 
			foreach ($value as $key1 => $value1) {
				echo "-".$value1.'<br>';
			}?>
			</p></td>
			<?php
			for ($j=1; $j <count($codes) ; $j++) { 
				if ($i == $j) {
					$c=$densitas_baru[$codes[$j]]*100;
					echo "<td >".number_format($c,2,'.','')."%</td>";
				}
			}
			$i++;
		}
		?>
			</tr>

		</tbody>
	</table>
	</div>
	<?php 
}else {	?>
	<h3>TIDAK TERDAPAT KEMUNGKINAN PENYAKIT LAIN</h3>
<?php  } ?>
<div class="col-12 row" id="produk" style="margin-top: 150px;padding: 0 0 ">
  <div class="col-5" style="margin-top: 10px">
    <hr style="background-color: black;">
  </div>
  <div class="col-2"><h4 style="text-align: center;">PERHITUNGAN MANUAL</h4></div>
  <div class="col-5"style="margin-top: 10px">
    <hr style="background-color: black">
      </div>
</div>
<hr>
<div class="col-12 row">
	<div class="col-3"></div>
	<div class="col-6">
		<h5 style="text-align: center;">GEJALA YANG DIPILIH:</h5>
<hr>
<?php 
$j=1;
for ($i=0; $i <count($var) ; $i++) { 
	$evidence[$i][0]=implode(',', $var[$i]);
	$evidence[$i][1]=$nilai1[$i];

}
	for ($i=0; $i <count($gj) ; $i++) { 
		echo "<p>".$j.". ".$gj[$i]."</p>";
		echo "<hr><p>Kode Penyakit:</p>";
		echo "<p>(".$evidence[$i][0].")</p>";
		echo "<hr><p>Nilai Densitas awal:</p>";
		echo "<p>Believe: ".$nilai1[$i]."</p>";
		echo "<p>Plausability: ". (1-$nilai1[$i])."</p><hr>";
		$j++;
	}
echo '<h5 style="text-align: center;">MENENTUKAN NILAI DENSITAS BARU:</h5>
<hr>';
$w=array();
foreach ($penyakit as $q) {
	$w[]='P'.$q->id;
}
$fod= implode(',', $w);
$densitas_baru=array();
$jj=0;
while(!empty($evidence)){
	$densitas1[0]=array_shift($evidence);
	$densitas1[1]=array($fod,1-$densitas1[0][1]);
	$densitas2=array();
	if(empty($densitas_baru)){
		$densitas2[0]=array_shift($evidence);
	}else{
		foreach($densitas_baru as $k=>$r){
			if($k!="&theta;"){
				$densitas2[]=array($k,$r);
			}
		}
	}
	$theta=1;
	foreach($densitas2 as $d) $theta-=$d[1];
	$densitas2[]=array($fod,$theta);
	$m=count($densitas2);
	$densitas_baru=array();
	echo "<label>Aturan Kombinasi untuk m".($jj+2)."</label><table class='table' style='border:1px solid black'>";
		echo "<tr><td></td>";
		echo "<td>m".($jj+1)."{".$densitas1[0][0]."}<br>".$densitas1[0][1]."</td><td>m".($jj+1)."{&theta;}<br>".$densitas1[1][1]."</td>";
	for($y=0;$y<$m;$y++){
		echo "<tr>";
		if (!($y==$m-1)) {
			$cc=$densitas2[$y][0];
		}
		else
		{
			$cc="&theta;";
		}
		echo "<td>m".($jj) ."{".$cc."}<br>".$densitas2[$y][1]."</td>";
		for($x=0;$x<2;$x++){
			if(!($y==$m-1 && $x==1)){
				$v=explode(',',$densitas1[$x][0]);
				$w=explode(',',$densitas2[$y][0]);
				sort($v);
				sort($w);
				$vw=array_intersect($v,$w);
				if(empty($vw)){
					$k="&theta;";
				}else{
					$k=implode(',',$vw);
				}
				if(!isset($densitas_baru[$k])){
					echo "<td>{".$k."}<br>".$densitas1[$x][1]*$densitas2[$y][1]."</td>";
					$densitas_baru[$k]=$densitas1[$x][1]*$densitas2[$y][1];
					$ss[$k]=$densitas1[$x][1]*$densitas2[$y][1]."+";
				}else{
					echo "<td>{".$k."}<br>".$densitas1[$x][1]*$densitas2[$y][1]."</td>";
					$densitas_baru[$k]+=$densitas1[$x][1]*$densitas2[$y][1];
					$ss[$k].=$densitas1[$x][1]*$densitas2[$y][1]."+";
				}
			}
			else{
		echo "<td>&theta;<br>".$densitas1[$x][1]*$densitas2[$y][1]."</td>";
			}
			
		}

		echo "</tr>";

	}
	echo "</table>";
	$cek=0;
	$tt=0;
	foreach($densitas_baru as $k=>$d){
		if($k=="&theta;"){
$cek=1;
$tt=$d;
		}

	$ss1[$k]=explode("+", $ss[$k]);
	}
	echo "<label>Dari perhitungan diatas didapatkan hasil untuk m".($jj+2).":</label><hr>";
	if ($cek==1) {
	echo "<p>Dikarekana adanya konflik pada perhitungan diatas maka k=".$tt."</p>";
	}
	else
	{
		echo "<p>Dikarekan tidak adanya konflik pada perhitungan diatas maka k=0</p>";
	}$jk='';$jkk=0;
	foreach($densitas_baru as $k=>$d){
		if($k!="&theta;"){
			if ((1-(isset($densitas_baru["&theta;"])?$densitas_baru["&theta;"]:0)) == 0) {
				redirect(site_url('pasien/pilih?cek=null'));
			}

			echo "m".($jj+2)."{".$k."} : ";
			echo "(".substr($ss[$k], 0, -1).") / 1 - ".$tt;
			echo " = ".$densitas_baru[$k]=$d/(1-(isset($densitas_baru["&theta;"])?$densitas_baru["&theta;"]:0));
			$jk.=$densitas_baru[$k]."+";
			$jkk+=$densitas_baru[$k];
			echo "<br>";

		}
	}
	echo "m".($jj+2)."{&theta;} : 1 - (".substr($jk, 0, -1).") = ".(1-$jkk) ;
	echo"<hr>";
		$jj+=2;
}
echo "<h5 style='text-align: center;'>HASIL PERANGKINGAN:</h5>
<hr>";
unset($densitas_baru["&theta;"]);
arsort($densitas_baru);
foreach ($densitas_baru as $key => $value) {
	echo "m".($jj)."{".$key."} : ".$value;;
	echo "<hr>";
}
$codes=array_keys($densitas_baru);
$val=array_values($densitas_baru);

?>
<label>Dari <b><?= count($var)?></b> gejala yang dipilih, didapat kombinasi penyakit atau hama dengan kode<b> <?= $codes[0]?></b> dengan persentasi kemungkinan <b><?= $val[0]*100?>%</b>, yaitu penyakit <b><?= $nama?></b>. </label>
	</div>
</div>

					
		<!-- /.container-fluid -->

		<!-- Sticky Footer -->

	</div>
	<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->
</div>
</div>
</main>
</div