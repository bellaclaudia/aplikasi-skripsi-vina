<main class="main">
  <ol class="breadcrumb">
    <li class="breadcrumb-item active">Pendaftaran Pasien</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Form Pendaftaran Pasien
        </div>
        <div class="card-body">
          <div class="form-group row">
              <div class="col-3">
                <h6>Gejala terpilih : <label style="font-weight: bold;" id="ck"></label></h6> 
              </div>
              <div class="col-9">
                <input type="checkbox" name="" id="checkAll" >
                <label>Pilih Semua Gejala</label>
              </div>
            </div>
          <form action="hasil" method="get" class="form-horizontal">
            
            <div class="form-group row">
              <label class="col-md-3 col-form-label">Gejala</label>
              <div class="col-md-9">
                <?php 
                $i=1;
                foreach ($gejala as $k) { ?>
                <input type="checkbox" name="id_gejala[]" value="<?= $k['id']; ?>" onclick="cek()"  id="cek<?php echo $i?>"> <?= $k['nama']; ?><br>
                <?php $i++;} ?>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" id="btn" title="Submit Pendaftaran">Kirim</button>&nbsp;
              <a href="<?= base_url('login'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
            </div>
          </form>
        </div>
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js" /></script>
        <script type="text/javascript">
          function cek(){
          checked1=$("input[type=checkbox]:checked").length;
          $("#ck").text(checked1);
          }
          $("#checkAll").click(function(){
            $('input:checkbox').not(this).prop('checked', this.checked);
              checked1=$("input[type=checkbox]:checked").length;
          $("#ck").text(checked1);
          });
          $(document).ready(function () {

            $('#btn').click(function () {
             checked=$("input[type=checkbox]:checked").length;
            if (!checked) {
              alert('Setidaknya Pilih 1 Gejala');
              return false;
            }
          });

          });
          <?php 
          if (isset($_GET['cek'])) {
            echo "alert('Pilih Gejala Terlebih Dahulu');";
          }
          ?>
        </script>
      </div>
    </div>
  </div>
</main>
</div>