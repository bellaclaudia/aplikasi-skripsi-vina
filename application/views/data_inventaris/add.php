<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('inventaris'); ?>">Data Inventaris</a></li>
    <li class="breadcrumb-item active">Tambah Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Tambah Data
        </div>
        <div class="card-body">
          <form id="FrmAdd" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
            <div class="form-group row">
              <label class="col-md-2 col-form-label">No. Inventaris</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="no_inventaris" placeholder="Isi No. Inventaris" value="<?= set_value('no_inventaris'); ?>">
                <small class="text-danger">
                  <?php echo form_error('no_inventaris') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Jumlah</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="jumlah" placeholder="Isi Jumlah" value="<?= set_value('jumlah'); ?>">
                <small class="text-danger">
                  <?php echo form_error('jumlah') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tgl. Awal</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_awal" value="<?= set_value('tgl_awal'); ?>">
                <small class="text-danger">
                  <?php echo form_error('tgl_awal') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Tgl. Akhir</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_akhir" value="<?= set_value('tgl_akhir'); ?>">
                <small class="text-danger">
                  <?php echo form_error('tgl_akhir') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Lokasi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_lokasi">
                  <option value="0" selected disabled>Pilih Lokasi</option>
                  <?php foreach ($lokasi as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_lokasi') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Nama Inventaris</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_inventaris">
                  <option value="0" selected disabled>Pilih Nama Inventaris</option>
                  <?php foreach ($inventaris as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_inventaris') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Ruangan</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-3" name="id_ruangan">
                  <option value="0" selected disabled>Pilih Ruangan</option>
                  <?php foreach ($ruangan as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_ruangan') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Nama Fisik</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-4" name="id_fisik">
                  <option value="0" selected disabled>Pilih Nama Fisik</option>
                  <?php foreach ($fisik as $k) : ?>
                    <option value="<?= $k['id']; ?>"><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_fisik') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Keterangan</label>
              <div class="col-md-10">
                <textarea class="form-control" name="keterangan" placeholder="Isi Keterangan"><?= set_value('keterangan'); ?></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
              <a href="<?= base_url('inventaris'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>