<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item"><a href="<?= base_url('inventaris'); ?>">Data Inventaris</a></li>
    <li class="breadcrumb-item active">Edit Data</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-plus"></i> Edit Data
        </div>
        <div class="card-body">
          <form id="FrmAdd" class="form-horizontal" method="post" enctype="multipart/form-data" autocomplete="off">
            <div class="form-group row">
              <label class="col-md-2 col-form-label">No. Inventaris</label>
              <div class="col-md-4">
              	<input class="form-control" type="hidden" name="id" value="<?= $data_inventaris->id; ?>">
                <input class="form-control" type="text" name="no_inventaris" placeholder="Isi No. Inventaris" value="<?= $data_inventaris->no_inventaris; ?>">
                <small class="text-danger">
                  <?php echo form_error('no_inventaris') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Jumlah</label>
              <div class="col-md-4">
                <input class="form-control" type="text" name="jumlah" placeholder="Isi Jumlah" value="<?= $data_inventaris->jumlah; ?>">
                <small class="text-danger">
                  <?php echo form_error('jumlah') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Tgl. Awal</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_awal" value="<?= $data_inventaris->tgl_awal; ?>">
                <small class="text-danger">
                  <?php echo form_error('tgl_awal') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Tgl. Akhir</label>
              <div class="col-md-4">
                <input class="form-control" type="date" name="tgl_akhir" value="<?= $data_inventaris->tgl_akhir; ?>">
                <small class="text-danger">
                  <?php echo form_error('tgl_akhir') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Lokasi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_lokasi">
                  	<option value="0" selected disabled>Pilih Lokasi</option>
                  	<?php
	                  $lv = '';
	                  if (isset($data_inventaris->id_lokasi)) {
	                    $lv = $data_inventaris->id_lokasi;
	                  }
	                  foreach ($lokasi as $r => $v) {
	                  ?>
	                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
	                  <?php
	                  }
                  	?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_lokasi') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Nama Inventaris</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_inventaris">
                  <option value="0" selected disabled>Pilih Nama Inventaris</option>
                  	<?php
	                  $lv = '';
	                  if (isset($data_inventaris->id_inventaris)) {
	                    $lv = $data_inventaris->id_inventaris;
	                  }
	                  foreach ($inventaris as $r => $v) {
	                  ?>
	                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
	                  <?php
	                  }
                  	?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_inventaris') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Ruangan</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-3" name="id_ruangan">
                  <option value="0" selected disabled>Pilih Ruangan</option>
                  	<?php
	                  $lv = '';
	                  if (isset($data_inventaris->id_ruangan)) {
	                    $lv = $data_inventaris->id_ruangan;
	                  }
	                  foreach ($ruangan as $r => $v) {
	                  ?>
	                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
	                  <?php
	                  }
                  	?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_ruangan') ?>
                </small>
              </div>
              <label class="col-md-2 col-form-label">Nama Fisik</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-4" name="id_fisik">
                  <option value="0" selected disabled>Pilih Nama Fisik</option>
                  	<?php
	                  $lv = '';
	                  if (isset($data_inventaris->id_fisik)) {
	                    $lv = $data_inventaris->id_fisik;
	                  }
	                  foreach ($fisik as $r => $v) {
	                  ?>
	                    <option value="<?= $v['id'] ?>" <?= $v['id'] == $lv ? 'selected' : '' ?>><?= $v['nama'] ?></option>
	                  <?php
	                  }
                  	?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_fisik') ?>
                </small>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Keterangan</label>
              <div class="col-md-10">
                <textarea class="form-control" name="keterangan" placeholder="Isi Keterangan"><?= $data_inventaris->keterangan; ?></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit"><i class="fa fa-plus"></i> Simpan</button>&nbsp;
              <a href="<?= base_url('inventaris'); ?>" class="btn btn-sm btn-danger btn-ladda" data-style="expand-right"><i class="fa fa-dot-circle-o"></i> Batal</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main>
</div>