<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Inventaris</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Inventaris
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <?php if ($this->session->flashdata('message')) :
              echo $this->session->flashdata('message');
            endif; ?>

            <b>Filter Pencarian</b>
            <?php
            $attributes = array('id' => 'FrmPencarian', 'method' => "post", "autocomplete" => "off");
            echo form_open('', $attributes);
            ?>
            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nama Lokasi</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-1" name="id_lokasi">
                  <option value="0">- All -</option>
                  <?php foreach ($lokasi as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_lokasi) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_lokasi') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Nama Inventaris</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-2" name="id_inventaris">
                  <option value="0">- All -</option>
                  <?php foreach ($inventaris as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_inventaris) { ?>selected<?php } ?>><?= $k['nama']; ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_inventaris') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Ruangan</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-3" name="id_ruangan">
                  <option value="0">- All -</option>
                  <?php foreach ($ruangan as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_ruangan) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_ruangan') ?>
                </small>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-2 col-form-label">Fisik</label>
              <div class="col-md-4">
                <select class="form-control select2-single" id="select2-4" name="id_fisik">
                  <option value="0">- All -</option>
                  <?php foreach ($fisik as $k) : ?>
                    <option value="<?= $k['id']; ?>" <?php if ($k['id'] == $id_fisik) { ?>selected<?php } ?>><?= $k['nama'] ?></option>
                  <?php endforeach; ?>
                </select>
                <small class="text-danger">
                  <?php echo form_error('id_fisik') ?>
                </small>
              </div>
            </div>

            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit">Cari</button>&nbsp;
            <button class="btn btn-sm btn-success btn-ladda" data-style="expand-right" type="submit" name="excel" value="1">Export Excel</button>
            </form>
            <hr>
            <?php if($this->session->userdata['id_grup_user'] == '1') : ?>
            <a href="<?= base_url('inventaris/tambah'); ?>"><button style="margin-left: 94%" class="btn btn-sm btn-success mb-1" type="button">Tambah Data</button></a><br><br>
            <?php endif ?>
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>QR Qode</th>
                  <th>No. Inventaris</th>
                  <th>Periode</th>
                  <th>Lokasi</th>
                  <th>Nama Inventaris</th>
                  <th>Ruangan</th>
                  <th>Fisik</th>
                  <th>Jumlah</th>
                  <th>Keterangan</th>
                  <?php if($this->session->userdata['id_grup_user'] == '1') : ?>
                  <th>Aksi</th>
                  <?php endif ?>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_inventaris as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><a href="<?php echo base_url().'assets/img/'.$row->qr_code;?>" download><img style="width: 100px;" src="<?php echo base_url().'assets/img/'.$row->qr_code;?>"></a></td>
                    <td><?= $row->no_inventaris ?></td>
                    <td><?= $row->tgl_awal ?> - <?= $row->tgl_akhir ?></td>
                    <td><?= $row->nama_lokasi ?></td>
                    <td><?= $row->nama_inventaris ?></td>
                    <td><?= $row->nama_ruangan ?></td>
                    <td><?= $row->nama_fisik ?></td>
                    <td><?= $row->jumlah ?></td>
                    <td><?= $row->keterangan ?></td>
                    <?php if($this->session->userdata['id_grup_user'] == '1') : ?>
                    <td>
                      <a href="<?= base_url('inventaris/edit/' . $row->id); ?>" class="btn btn-success btn-circle"><i class="fa fa-edit"></i></a>
                      <a href="<?php echo site_url('inventaris/hapus/' . $row->id); ?>" onclick="return confirm('Apakah Anda Ingin Menghapus Data Inventaris No. <?= $row->no_inventaris; ?> ?');" class="btn btn-danger btn-circle" data-popup="tooltip" data-placement="top" title="Hapus Data"><i class="fa fa-trash"></i></a>
                    </td>
                    <?php endif ?>
                  </tr>
                <?php endforeach; ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>