<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?= base_url(); ?>">Home</a></li>
    <li class="breadcrumb-item">
      <a href="<?= base_url('dashboard'); ?>">Admin</a>
    </li>
    <li class="breadcrumb-item active">Data Inventaris</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      <div class="card">
        <div class="card-header">
          <i class="fa fa-list"></i> Data Inventaris
        </div>
        <div style="overflow-x:auto;">
          <div class="card-body">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>No. Inventaris</th>
                  <th>Periode</th>
                  <th>Lokasi</th>
                  <th>Nama Inventaris</th>
                  <th>Ruangan</th>
                  <th>Fisik</th>
                  <th>Jumlah</th>
                  <th>Keterangan</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($data_inventaris as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->no_inventaris ?></td>
                    <td><?= $row->tgl_awal ?> - <?= $row->tgl_akhir ?></td>
                    <td><?= $row->nama_lokasi ?></td>
                    <td><?= $row->nama_inventaris ?></td>
                    <td><?= $row->nama_ruangan ?></td>
                    <td><?= $row->nama_fisik ?></td>
                    <td><?= $row->jumlah ?></td>
                    <td><?= $row->keterangan ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
</div>