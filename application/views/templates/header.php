<!DOCTYPE html>
<html lang="en">

<head>
  <base href="./">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <meta name="description" content="SISTEM PAKAR">
  <meta name="author" content="Bella Claudia">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
  <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
  <title>.:: SISTEM PAKAR ::.</title>
  <!-- Icons-->
  <link href="<?= base_url(); ?>assets/node_modules/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
  <!-- Main styles for this application-->
  <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/css/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
  <link href="<?= base_url(); ?>assets/vendors/select2/css/select2.min.css" rel="stylesheet">
  <link href="<?= base_url(); ?>assets/node_modules/ladda/dist/ladda-themeless.min.css" rel="stylesheet">
</head>

<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
  <header class="app-header navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="<?= base_url(); ?>">
      SISTEM PAKAR
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
      <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="nav navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <img class="img-avatar" src="<?= base_url(); ?>assets/img/user.png" alt="admin@iaiqi.ac.id">
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          <div class="dropdown-header text-center">
            <strong>Hi, <?php if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) echo "anonymous";
                        else echo $this->session->userdata['username']; ?> </strong>
          </div>

          <?php if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) { ?>
            <a class="dropdown-item" href="<?= base_url('login') ?>">
              <i class="fa fa-lock"></i> Login
            </a>
          <?php } else { ?>
            <a class="dropdown-item" href="<?= base_url('login/logout') ?>">
              <i class="fa fa-lock"></i> Logout
            </a>
          <?php } ?>
        </div>
      </li>
    </ul>
  </header>