<div class="app-body">
  <div class="sidebar">
    <nav class="sidebar-nav">
      <ul class="nav">
        <li class="nav-title">Menu</li>
        <?php if (isset($this->session->userdata['logged_in'])) { // jika udh lgin 
        ?>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('dashboard'); ?>">
              <i class="nav-icon icon-speedometer"></i> Dashboard
            </a>
          </li>
          <?php if ($this->session->userdata['id_grup_user'] == '1') { // administrator 
          ?>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-list"></i> Master</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('penyakit'); ?>">
                    <i class="nav-icon fa fa-list"></i> Penyakit
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('penanganan'); ?>">
                    <i class="nav-icon fa fa-list"></i> Penanganan
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('gejala'); ?>">
                    <i class="nav-icon fa fa-list"></i> Gejala
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('diagnosa'); ?>">
                    <i class="nav-icon fa fa-list"></i> Diagnosa
                  </a>
                </li>
              </ul>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('riwayat'); ?>">
                <i class="nav-icon icon-list"></i> Riwayat Konsultasi
              </a>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-share-alt"></i> Pengaturan</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_grup_user'); ?>">
                    <i class="nav-icon fa fa-users"></i> Grup User
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('master_user'); ?>">
                    <i class="nav-icon fa fa-user"></i> Data User
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('ganti_pass') ?>">
                    <i class="nav-icon fa fa-users"></i> Ganti Password
                  </a>
                </li>
              </ul>
            </li>
          <?php } else if ($this->session->userdata['id_grup_user'] == '2') { // staf akademik 
          ?>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('inventaris'); ?>">
                <i class="nav-icon icon-list"></i> Data Inventaris
              </a>
            </li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon fa fa-share-alt"></i> Pengaturan</a>
              <ul class="nav-dropdown-items">
                <li class="nav-item">
                  <a class="nav-link" href="<?= base_url('ganti_pass') ?>">
                    <i class="nav-icon fa fa-users"></i> Ganti Password
                  </a>
                </li>
              </ul>
            </li>
        <?php }
        }
        ?>
      </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
  </div>