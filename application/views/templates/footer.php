    <footer class="app-footer">
      <div>
        <a href="<?= base_url('dashboard'); ?>">.:: SISTEM PAKAR ::.</a>
        <span><?=date('Y')?> &copy;.</span>
      </div>
    </footer>
    <!-- CoreUI and necessary plugins-->
    <script src="<?= base_url(); ?>assets/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/pace-progress/pace.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/@coreui/coreui-pro/dist/js/coreui.min.js"></script>
    <!-- Plugins and scripts required by this view-->
    <script src="<?= base_url(); ?>assets/node_modules/chart.js/dist/Chart.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/main.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/jquery.maskedinput/src/jquery.maskedinput.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/moment/min/moment.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/select2/dist/js/select2.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?= base_url(); ?>assets/js/advanced-forms.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/ladda/dist/spin.min.js"></script>
    <script src="<?= base_url(); ?>assets/node_modules/ladda/dist/ladda.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/loading-buttons.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.dataTables.js"></script>
    <script src="<?= base_url(); ?>assets/js/dataTables.bootstrap4.js"></script>
    <script src="<?= base_url(); ?>assets/js/datatables.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery-1.js"></script>
    <script src="<?= base_url(); ?>assets/js/sb-admin.min.js"></script>
  </body>
</html>