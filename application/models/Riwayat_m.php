<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Riwayat_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'riwayat';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       

    public function getAll()
    {
        $this->db->select('a.*');
        $this->db->from('riwayat a');
        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query;
    }

    public function getById($id)
    {
        $this->db->select('a.id, a.tanggal, a.persen, a.id_alergi');
        $this->db->from('riwayat a');
        $this->db->where("a.id", $id);
        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query;
    }

    public function getAlergi()
    {
        return $this->db->get('jenis_alergi');
    }

    public function save()
    {
        $data = array(
            "nama" => $this->input->post('nama'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
