<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penanganan_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'penanganan';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_penyakit',
                'label' => 'Alergi',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'penanganan',
                'label' => 'Penanganan Penyakit',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->select('a.*, b.nama as nama_penyakit');
        $this->db->from('penanganan a');
        $this->db->join('penyakit b', 'b.id = a.id_penyakit', 'left');
        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getPenyakit()
    {
        return $this->db->get('penyakit')->result_array();
    }

    public function save()
    {
        $data = array(
            "id_penyakit" => $this->input->post('id_penyakit'),
            "penanganan" => $this->input->post('penanganan'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
