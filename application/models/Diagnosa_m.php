<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Diagnosa_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'diagnosa';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_alergi',
                'label' => 'Alergi',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_gejala',
                'label' => 'Gejala',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'bobot',
                'label' => 'Bobot Kepercayaan',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->select('a.*, b.nama as nama_alergi, c.nama as nama_gejala');
        $this->db->from('diagnosa a');
        $this->db->join('jenis_alergi b', 'b.id = a.id_alergi', 'left');
        $this->db->join('gejala c', 'a.id_gejala = c.id', 'left');
        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getAlergi()
    {
        return $this->db->get('jenis_alergi')->result_array();
    }

    public function getGejala()
    {
        return $this->db->get('gejala')->result_array();
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "id_alergi" => $this->input->post('id_alergi'),
            "id_gejala" => $this->input->post('id_gejala'),
            "bobot" => $this->input->post('bobot'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
