<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Data_inventaris_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'inventaris';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_lokasi',
                'label' => 'Lokasi',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_inventaris',
                'label' => 'Nama Inventaris',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_ruangan',
                'label' => 'Ruangan',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'id_fisik',
                'label' => 'Fisik',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'no_inventaris',
                'label' => 'No. Inventaris',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_awal',
                'label' => 'Tgl. Awal',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_akhir',
                'label' => 'Tgl. Akhir',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'jumlah',
                'label' => 'Jumlah',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll($id_lokasi, $id_inventaris, $id_ruangan, $id_fisik)
    {
        $this->db->select('a.*, b.nama as nama_lokasi, c.nama as nama_inventaris, d.nama as nama_ruangan, e.nama as nama_fisik');
        $this->db->from('inventaris a');
        $this->db->join('master_lokasi b', 'b.id = a.id_lokasi', 'left');
        $this->db->join('master_inventaris c', 'a.id_inventaris = c.id', 'left');
        $this->db->join('master_ruangan d', 'd.id = a.id_ruangan', 'left');
        $this->db->join('master_fisik e', 'e.id = a.id_fisik', 'left');
        $this->db->order_by("a.id", "desc");

        if ($id_lokasi != '0')
            $this->db->where("a.id_lokasi", $id_lokasi);
        if ($id_inventaris != '0')
            $this->db->where("a.id_inventaris", $id_inventaris);
        if ($id_ruangan != '0')
            $this->db->where("a.id_ruangan", $id_ruangan);
        if ($id_fisik != '0')
            $this->db->where("a.id_fisik", $id_fisik);

        $query = $this->db->get();
        return $query->result();
    }

    public function getDataView($no_inventaris)
    {
        $this->db->select('a.*, b.nama as nama_lokasi, c.nama as nama_inventaris, d.nama as nama_ruangan, e.nama as nama_fisik');
        $this->db->from('inventaris a');
        $this->db->join('master_lokasi b', 'b.id = a.id_lokasi', 'left');
        $this->db->join('master_inventaris c', 'a.id_inventaris = c.id', 'left');
        $this->db->join('master_ruangan d', 'd.id = a.id_ruangan', 'left');
        $this->db->join('master_fisik e', 'e.id = a.id_fisik', 'left');
        $this->db->order_by("a.id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $this->load->library('ciqrcode'); //pemanggilan library QR CODE
 
        $config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/img/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);
        
        $timestamp = date('YmdHis');
        $image_name='Barcode_'.$timestamp.'.png';
        // $kode_unik = 'DataInventaris_' . $timestamp;
        $link = base_url('inventaris/data_inventaris/' . $this->input->post('no_inventaris'));

        $params['data'] = $link; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
        $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE

        $data = array(
            "id_lokasi" => $this->input->post('id_lokasi'),
            "id_inventaris" => $this->input->post('id_inventaris'),
            "id_ruangan" => $this->input->post('id_ruangan'),
            "id_fisik" => $this->input->post('id_fisik'),
            "no_inventaris" => $this->input->post('no_inventaris'),
            "tgl_awal" => $this->input->post('tgl_awal'),
            "tgl_akhir" => $this->input->post('tgl_akhir'),
            "jumlah" => $this->input->post('jumlah'),
            "keterangan" => $this->input->post('keterangan'),
            "qr_code" => $image_name,
            "link" => $link,
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }

    public function getLokasi()
    {
        return $this->db->get('master_lokasi')->result_array();
    }

    public function getInventaris()
    {
        return $this->db->get('master_inventaris')->result_array();
    }

    public function getRuangan()
    {
        return $this->db->get('master_ruangan')->result_array();
    }

    public function getFisik()
    {
        return $this->db->get('master_fisik')->result_array();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->table, ["id" => $id])->row();
    }

    public function update()
    {
        $this->load->library('ciqrcode'); //pemanggilan library QR CODE
 
        $config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/img/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);
 
        $timestamp = date('YmdHis');
        $image_name='Barcode_'.$timestamp.'.png';
        // $kode_unik = 'DataInventaris_' . $timestamp;
        $link = base_url('inventaris/data_inventaris/' . $this->input->post('no_inventaris'));

        if (!empty($image_name)) {
            $params['data'] = $link; //data yang akan di jadikan QR CODE
            $params['level'] = 'H'; //H=High
            $params['size'] = 10;
            $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
            $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
        } else {
            $this->db->select('qr_code');
            $this->db->where('id', $this->input->post('id'));
            $q = $this->db->get('undangan')->row_array();
            $image_name = $q['qr_code'];
        }

        $data = array(
            "id_lokasi" => $this->input->post('id_lokasi'),
            "id_inventaris" => $this->input->post('id_inventaris'),
            "id_ruangan" => $this->input->post('id_ruangan'),
            "id_fisik" => $this->input->post('id_fisik'),
            "no_inventaris" => $this->input->post('no_inventaris'),
            "tgl_awal" => $this->input->post('tgl_awal'),
            "tgl_akhir" => $this->input->post('tgl_akhir'),
            "jumlah" => $this->input->post('jumlah'),
            "keterangan" => $this->input->post('keterangan'),
            "qr_code" => $image_name,
            "link" => $link,
            "tgl_update" => date('Y-m-d H:i:s', strtotime('-8 minutes')),
            "user_update_by" => $this->session->userdata['username']
        );

        return $this->db->update($this->table, $data, array('id' => $this->input->post('id')));
    }
}
