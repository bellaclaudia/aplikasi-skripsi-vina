<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pasien_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'pasien';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'nama',
                'label' => 'Nama Lengkap',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'alamat',
                'label' => 'Alamat',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tmp_lahir',
                'label' => 'Tempat Lahir',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'tgl_lahir',
                'label' => 'Tanggal Lahir',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama_ibu',
                'label' => 'Nama Ibu',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
        //fungsi diatas seperti halnya query 
        //select * from mahasiswa order by IdMhsw desc
    }

    public function getPasien($id='')
    {
        return $this->db->get('pasien');
    }

    public function getGejala()
    {
        return $this->db->get('gejala');
    }

    public function getPenyakit()
    {
        $this->db->select('a.*, b.penanganan');
        $this->db->from('penyakit a');
        $this->db->join('penanganan b','b.id_penyakit = a.id','left');
        $query = $this->db->get();
        return $query;
    }

    public function getDiagnosa()
    {
        return $this->db->get('diagnosa')->result();
    }

    public function insert($data){
        $this->db->insert('riwayat',$data);
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "nama" => $this->input->post('nama'),
            "alamat" => $this->input->post('alamat'),
            "tmp_lahir" => $this->input->post('tmp_lahir'),
            "tgl_lahir" => $this->input->post('tgl_lahir'),
            "nama_ibu" => $this->input->post('nama_ibu'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }

    public function saveBerobat()
    {
        $id_diagnosa = implode(',', $_POST['id_diagnosa']);

        $data = array(
            "id_pasien" => $this->input->post('id_pasien'),
            "id_diagnosa" => $id_diagnosa,
            "tgl_berobat" => date('Y-m-d H:i:s')
        );
        return $this->db->insert('riwayat', $data);
    }
}
