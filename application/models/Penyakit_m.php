<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penyakit_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'penyakit';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'kode',
                'label' => 'Kode',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'nama',
                'label' => 'Penyakit',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'definisi',
                'label' => 'Definisi',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data mahasiswa
    public function getAll()
    {
        $this->db->from($this->table);
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
        //fungsi diatas seperti halnya query 
        //select * from mahasiswa order by IdMhsw desc
    }

    //menyimpan data mahasiswa
    public function save()
    {
        $data = array(
            "kode" => $this->input->post('kode'),
            "definisi" => $this->input->post('definisi'),
            "nama" => $this->input->post('nama'),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
