<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_user_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    private $table = 'master_user';

    //validasi form, method ini akan mengembailkan data berupa rules validasi form       
    public function rules()
    {
        return [
            [
                'field' => 'id_grup_user',  //samakan dengan atribute name pada tags input
                'label' => 'ID Fakultas',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'username',  //samakan dengan atribute name pada tags input
                'label' => 'username',  // label yang kan ditampilkan pada pesan error
                'rules' => 'trim|required' //rules validasi
            ],
            [
                'field' => 'userpass',
                'label' => 'Nama Prodi',
                'rules' => 'trim|required'
            ]
        ];
    }

    //menampilkan semua data 
    public function getAll()
    {
        $this->db->select('a.*, b.nama_grup');
        $this->db->from('master_user a');
        $this->db->join('master_grup_user b', 'b.id = a.id_grup_user', 'left');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getGrupUser()
    {
        return $this->db->get('master_grup_user')->result_array();
    }

    //menyimpan data
    public function save()
    {
        $data = array(
            "id_grup_user" => $this->input->post('id_grup_user'),
            "username" => $this->input->post('username'),
            "userpass" => md5($this->input->post('userpass')),
            "tgl_input" => date('Y-m-d H:i:s'),
            "user_update_by" => $this->session->userdata['username']
        );
        return $this->db->insert($this->table, $data);
    }
}
