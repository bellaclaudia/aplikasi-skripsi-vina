<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Riwayat extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("riwayat_m");

        require_once APPPATH.'third_party/fpdf/fpdf.php';
        
        $pdf = new FPDF();
        $pdf->AddPage();
        
        $CI =& get_instance();
        $CI->fpdf = $pdf;

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $data["riwayat"] = $this->riwayat_m->getAll()->result();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('riwayat/index', $data);
        $this->load->view('templates/footer');
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Riwayat Konsultasi gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('riwayat');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('riwayat');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Riwayat Konsultasi berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('riwayat');
        }
    }

    public function laporan($id){
	  $no=1;

	$id= $id;

	$cek=$this->riwayat_m->getById($id)->result();
	foreach ($cek as $q) {
	  if ($q->id == $id) {
	    $tanggal=$q->tanggal;
	    $persen=$q->persen;
	    $pen=$q->id_alergi;

		  $cek1=$this->riwayat_m->getAlergi()->result();
		    foreach ($cek1 as $w) {
		      if ($pen == $w->id) {
		        $nama=$w->nama;
		      }
		    }
		  }
		}
	    $pdf = new FPDF('P','mm','A4'); //L = lanscape P= potrait
	        $pdf->SetMargins(10 ,20, 10 );
	        $pdf->AddPage();
	        // setting jenis font yang akan digunakan
	        $pdf->SetFont('Arial','B',14);
            $pdf->Cell(0,7,'Laporan Hasil Konsultasi',0,1,'C');
            $pdf->SetFont('Arial','B',13);
            $pdf->SetFont('Arial','',12);
	        $pdf->Cell(0,11,'Tanggal : '.$tanggal,0,1,'C');
	        $pdf->SetLineWidth(1);
	        $pdf->SetDrawColor(1,1,1);
	        $pdf->line(10,40,200,40);
	        $pdf->SetLineWidth(0);
	        $pdf->SetLineWidth(0.5);
	        $pdf->SetDrawColor(1,1,1);
	        $pdf->SetLineWidth(0);
	        $pdf->SetDrawColor(1,1,1);
	        $pdf->line(30,160,180,160); 
	        $pdf->SetFont('Arial','B',11);
	        $pdf->Cell(280,7,'',0,1,'C');
	        $pdf->Cell(10,10,'',0,1);
	        $pdf->SetFont('Arial','B',11);
	        $pdf->Cell(25,10,'',0,0,'L');
	        $pdf->Cell(25,10,'',0,0,'L');
	        $pdf->Cell(85,10,'',0,1,'L');
	        $pdf->Cell(190,10,'HASIL KONSULTASI',1,1,'C');

	        $pdf->Cell(85,10,'',0,1,'L');
	        $pdf->Cell(85,10,'',0,0,'L');
	        $pdf->Cell(20,10,'Nama Alergi : ',0,1,'C');
	        $pdf->Cell(85,50,'',0,0);
	        $pdf->SetFont('Arial','',11);
	        $pdf->Cell(20,10,$nama,0,1,'C');
	        $pdf->Cell(85,5,'',0,1,'L');
	        $pdf->SetFont('Arial','',9);
	        $pdf->Cell(85,10,'',0,0,'L');
	        $pdf->Cell(20,10,'Persentasi kepercayaan untuk hasil akhir yang diberikan adalah sebesar: ',0,1,'C');
	        $pdf->Cell(85,10,'',0,0,'L');
	        $pdf->SetFont('Arial','B',10);
	        $pdf->Cell(20,10,$persen.'%',0,1,'C');
	        $pdf->Cell(85,15,'',0,1,'L');
	        $pdf->Cell(85,10,'',0,0,'L');
	        $pdf->SetFont('Arial','B',11);
	        $pdf->Cell(20,10,'PENANGANAN',0,1,'C');
	        $pdf->SetFont('Arial','',11);
	        $cek = $this->riwayat_m->getAlergi()->result();
	        foreach ($cek as $q) {
	          if ($q->id==$pen) {
	        $pdf->Cell(5,7,'-',0,0,'L');
	        $pdf->MultiCell( 190, 7, $q->penanganan, 0);
	      }
	        }
	       
	 $pdf->Output();
	}  
}
