<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Diagnosa extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("diagnosa_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $diagnosa = $this->diagnosa_m;
        $validation = $this->form_validation;
        $validation->set_rules($diagnosa->rules());
        if ($validation->run()) {
            $diagnosa->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Diagnosa berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("diagnosa");
        }
        $data["title"] = "Diagnosa";
        $data["data_diagnosa"] = $this->diagnosa_m->getAll();
        $data['alergi'] = $this->diagnosa_m->getAlergi();
        $data["gejala"] = $this->diagnosa_m->getGejala();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('diagnosa/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('id_alergi', 'id_alergi', 'required');
        $this->form_validation->set_rules('id_gejala', 'id_gejala', 'required');
        $this->form_validation->set_rules('bobot', 'bobot', 'required');
        $data['alergi'] = $this->diagnosa_m->getAlergi();
        $data["gejala"] = $this->diagnosa_m->getGejala();
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Diagnosa gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('diagnosa');
        } else {
            $data = array(
                "id" => $_POST['id'],
                "id_alergi" => $_POST['id_alergi'],
                "id_gejala" => $_POST['id_gejala'],
                "bobot" => $_POST['bobot'],
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $_POST['id']);
            $this->db->update('diagnosa', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Diagnosa berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('diagnosa');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Diagnosa gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('diagnosa');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('diagnosa');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Diagnosa berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('diagnosa');
        }
    }
}
