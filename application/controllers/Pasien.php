<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pasien extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("pasien_m");
        require_once APPPATH.'third_party/fpdf/fpdf.php';
        
        $pdf = new FPDF();
        $pdf->AddPage();
        
        $CI =& get_instance();
        $CI->fpdf = $pdf;
        // if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
        //     redirect('login');
        // }
    }

    public function index()
    {
        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }

        $pasien = $this->pasien_m;
        $validation = $this->form_validation;
        $validation->set_rules($pasien->rules());
        if ($validation->run()) {
            $pasien->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pasien berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("pasien");
        }
        $data["title"] = "Data Pasien";
        $data['data_pasien'] = $this->pasien_m->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('pasien/index', $data);
        $this->load->view('templates/footer');
    }

    public function pendaftaran()
    {
        $data['gejala'] = $this->pasien_m->getGejala()->result_array();
        $data["title"] = "Pendaftaran Pasien";
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('pasien/pendaftaran', $data);
        $this->load->view('templates/footer');
    }

    public function daftar()
    {
        $checkbox = $this->input->post('id_diagnosa');
        // $kode = "P-".date('YmdHis');
        $data_urut = $this->db->query("SELECT kode FROM riwayat order by id desc limit 1")->row_array();
        $sqlxx = "SELECT kode FROM riwayat order by id desc limit 1";
        $queryxx = $this->db->query($sqlxx);
        if ($queryxx->num_rows() > 0) {
            $hasilxx = $queryxx->row();
            $urutan    = $hasilxx->kode;
            $urutan++;
            $kode = sprintf("%03s", $urutan);
        }else{
            $hasilxx = $queryxx->row();
            $urutan = "000";
            $urutan++;
            $kode = sprintf("%03s", $urutan);
        }
        $urutan = 0;

        foreach ($checkbox as $key) {
            $urutan++;
            
            $jml = $data_awal['jml'];
            $total = $data_awal['total'];
            
            $hasil_akhir = 0;
            $hasil_akhir += $hasil;
            $nilai_gejala = round($hasil_akhir,2);
            $nilai_gejala_full = round($nilai_gejala,2);
            $total_key = 0;
            $total_key += $key;
            $seluruh = $nilai_gejala_full/$total_key;
            $hasil_final = round($seluruh,2);

            // if($urutan = 1 OR $urutan = 2){
                $hasil = 0;
                $hasil = $total/$jml;
                $hasil_kurang = 0;
                $hasil_kurang = 1-$hasil;
            // }

            if($urutan == 1){
                $data = $this->db->query("SELECT * FROM riwayat where kode = '$kode' and $urutan = 1")->row_array();
                $hasil = 0;
                $hasil = $total/$jml;
                $hasil_kurang = 0;
                $hasil_kurang = 1-$hasil;

                $hasil = 0;
                $hasil = $total/$jml;
                $hasil_kurang = 0;
                $hasil_kurang = 1-$hasil;
            }elseif($urutan == 2){
                $hasil = 0;
                $hasil = $total/$jml;
                $hasil_kurang = 0;
                $hasil_kurang = 1-$hasil;
            }else{
                $hasil2 = 0;
                $hasil2 = $total/$jml;
                $hasil_kurang2 = 0;
                $hasil_kurang2 = 1-$hasil;
            }

            $this->pasien_m->insert(array(
                'kode' => $kode,
                'urutan' => $urutan,
                'id_pasien' => $this->input->post('id_pasien'),
                'id_diagnosa' => $key,
                'nilai_gejala' => $hasil,
                'total_gejala' => $hasil_kurang,
                'tgl_berobat' => date('Y-m-d H:i:s')
            ));
        }

        $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
        Data Gejala Pasien berhasil disimpan. 
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button></div>');
        redirect('pasien/pendaftaran');
    }

    public function hasil()
    {
        $data['penyakit']=$this->pasien_m->getPenyakit()->result();
        $data['gejala']=$this->pasien_m->getDiagnosa();
        $data['g']=$this->pasien_m->getGejala()->result();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('pasien/hasil', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('alamat', 'alamat', 'required');
        $this->form_validation->set_rules('tmp_lahir', 'tmp_lahir', 'required');
        $this->form_validation->set_rules('tgl_lahir', 'tgl_lahir', 'required');
        $this->form_validation->set_rules('nama_ibu', 'nama_ibu', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pasien gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('pasien');
        } else {
            $data = array(
                "id" => $_POST['id'],
                "nama" => $_POST['nama'],
                "alamat" => $_POST['alamat'],
                "tmp_lahir" => $_POST['tmp_lahir'],
                "tgl_lahir" => $_POST['tgl_lahir'],
                "nama_ibu" => $_POST['nama_ibu'],
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $_POST['id']);
            $this->db->update('pasien', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pasien berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('pasien');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pasien gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('pasien');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('pasien');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Pasien berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('pasien');
        }
    }

    public function laporan(){
    $no=1;
    $pen=$this->input->get('pen');
    $alergi = $this->pasien_m->getAlergi($pen)->row_array();
    
    $nama = $alergi['nama'];
    $persen=$this->input->get('persen');
    $tanggal=date('Y-m-d');

    $data['persen']=$persen;
    $data['id_alergi']=$pen;
    $data['tanggal']=$tanggal;
    $this->pasien_m->insert($data,'riwayat');
        $pdf = new FPDF('P','mm','A4'); //L = lanscape P= potrait
            $pdf->SetMargins(10 ,10, 10 );
            $pdf->AddPage();
            // setting jenis font yang akan digunakan
            $pdf->SetFont('Arial','B',14);
            $pdf->Cell(0,7,'Laporan Hasil Konsultasi',0,1,'C');
            $pdf->SetFont('Arial','B',13);
            $pdf->SetFont('Arial','',12);
            $pdf->Cell(0,11,'Tanggal: '.DATE('d').' '.DATE('M').' '.DATE('Y'),0,1,'C');
            $pdf->SetLineWidth(1);
            $pdf->SetDrawColor(1,1,1);
            $pdf->line(10,40,200,40);
            $pdf->SetLineWidth(0);
            $pdf->SetLineWidth(0.5);
            $pdf->SetDrawColor(1,1,1);
            $pdf->SetLineWidth(0);
            $pdf->SetDrawColor(1,1,1);
            $pdf->line(30,160,180,160); 
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(280,7,'',0,1,'C');
            $pdf->Cell(10,10,'',0,1);
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(25,10,'',0,0,'L');
            $pdf->Cell(25,10,'',0,0,'L');
            $pdf->Cell(85,10,'',0,1,'L');
            $pdf->Cell(190,10,'HASIL KONSULTASI',1,1,'C');

            $pdf->Cell(85,10,'',0,1,'L');
            $pdf->Cell(85,10,'',0,0,'L');
            $pdf->Cell(20,10,'Jenis Alergi : ',0,1,'C');
            $pdf->Cell(85,50,'',0,0);
            $pdf->SetFont('Arial','',11);
            $pdf->Cell(20,10,$nama,0,1,'C');
            $pdf->Cell(85,5,'',0,1,'L');
            $pdf->SetFont('Arial','',9);
            $pdf->Cell(85,10,'',0,0,'L');
            $pdf->Cell(20,10,'Persentasi kepercayaan untuk hasil akhir yang diberikan adalah sebesar: ',0,1,'C');
            $pdf->Cell(85,10,'',0,0,'L');
            $pdf->SetFont('Arial','B',10);
            $pdf->Cell(20,10,$persen.'%',0,1,'C');
            $pdf->Cell(85,15,'',0,1,'L');
            $pdf->Cell(85,10,'',0,0,'L');
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(20,10,'PENANGANAN',0,1,'C');
            $pdf->SetFont('Arial','',11);
            $cek = $this->pasien_m->getAlergi()->result();
            foreach ($cek as $q) {
              if ($q->id==$pen) {
            $pdf->Cell(5,7,'-',0,0,'L');
            $pdf->MultiCell( 190, 7, $q->penanganan, 0);
          }
            }
           
     $pdf->Output();
    }  
}
