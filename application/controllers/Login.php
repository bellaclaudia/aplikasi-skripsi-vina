<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
			$this->load->view('login');
		} else
			redirect('dashboard');
	}

	public function proses_login()
	{
		$this->form_validation->set_rules('username', 'username', 'required', ['required' => 'Username harus diisi..!']);
		$this->form_validation->set_rules('password', 'password', 'required', ['required' => 'Password harus diisi..!']);

		if ($this->form_validation->run() == FALSE) {
			// echo "kesini";
			// die();
			//$this->load->view('login');
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            Maaf, username atau password anda salah. Silahkan coba lagi
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');

			// $this->session->set_flashdata('pesan', 'Maaf, username atau password anda salah. Silahkan coba lagi');
			redirect('login');
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$user = $username;
			$pass = md5($password);

			$cek = $this->login_model->cek_login($user, $pass);

			if ($cek->num_rows() > 0) {
				foreach ($cek->result() as $ck) {
					$sess_data['username'] = $ck->username;
					$sess_data['id_grup_user'] = $ck->id_grup_user;
					$sess_data['logged_in'] = true;

					$this->session->set_userdata($sess_data);

					// 16-10-2021
					$data = array(
						"status_login" => '1',
						"last_login" => date('Y-m-d H:i:s'),
						"last_logout" => null
					);
					$this->db->where('username', $ck->username);
					$this->db->update('master_user', $data);
				}

				redirect('dashboard');
			} else {
				// $this->session->set_flashdata('pesan', 'Maaf, username atau password anda salah. Silahkan coba lagi');
				$this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
	            Maaf, username atau password anda salah. Silahkan coba lagi
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	            <span aria-hidden="true">&times;</span>
	            </button></div>');
				redirect('login');
			}
		}
	}

	public function logout()
	{
		// 16-10-2021
		$data = array(
			"status_login" => '0',
			"last_logout" => date('Y-m-d H:i:s'),
			"last_login" => null
		);
		$this->db->where('username', $this->session->userdata['username']);
		$this->db->update('master_user', $data);

		$this->session->sess_destroy();
		redirect('login');
	}
}
