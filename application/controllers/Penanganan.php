<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penanganan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("penanganan_m");

        if (!isset($this->session->userdata['logged_in']) || $this->session->userdata['logged_in'] != true) {
            redirect('login');
        }
    }

    public function index()
    {
        $penanganan = $this->penanganan_m;
        $validation = $this->form_validation;
        $validation->set_rules($penanganan->rules());
        if ($validation->run()) {
            $penanganan->save();
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Penanganan Penyakit berhasil disimpan. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect("penanganan");
        }
        $data["title"] = "penanganan";
        $data["data_penanganan"] = $this->penanganan_m->getAll();
        $data['penyakit'] = $this->penanganan_m->getPenyakit();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/menu');
        $this->load->view('penanganan/index', $data);
        $this->load->view('templates/footer');
    }

    public function edit($id = null)
    {
        $this->form_validation->set_rules('id_penyakit', 'id_penyakit', 'required');
        $this->form_validation->set_rules('penanganan', 'penanganan', 'required');
        $data['penyakit'] = $this->penanganan_m->getPenyakit();
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Penanganan Penyakit gagal diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('penanganan');
        } else {
            $data = array(
                "id" => $_POST['id'],
                "id_penyakit" => $_POST['id_penyakit'],
                "penanganan" => $_POST['penanganan'],
                "tgl_update" => date('Y-m-d H:i:s'),
                "user_update_by" => $this->session->userdata['username']
            );
            $this->db->where('id', $_POST['id']);
            $this->db->update('penanganan', $data);
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Penanganan Penyakit berhasil diedit. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('penanganan');
        }
    }

    public function hapus($id)
    {
        if ($id == "") {
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data Penanganan Penyakit gagal dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('penanganan');
        } else {
            $this->db->where('id', $id);
            $this->db->delete('penanganan');
            $this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">
            Data penanganan berhasil dihapus. 
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button></div>');
            redirect('penanganan');
        }
    }
}
