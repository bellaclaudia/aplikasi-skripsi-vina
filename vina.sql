-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 10 Agu 2022 pada 11.33
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vina`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `diagnosa`
--

CREATE TABLE `diagnosa` (
  `id` int(11) NOT NULL,
  `id_alergi` int(11) NOT NULL,
  `id_gejala` int(11) NOT NULL,
  `bobot` varchar(5) NOT NULL,
  `tgl_input` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `user_update_by` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `diagnosa`
--

INSERT INTO `diagnosa` (`id`, `id_alergi`, `id_gejala`, `bobot`, `tgl_input`, `tgl_update`, `user_update_by`) VALUES
(83, 1, 1, '0.6', '2022-06-25 04:18:39', '0000-00-00 00:00:00', 'admin'),
(84, 1, 2, '0.8', '2022-06-25 04:18:48', '0000-00-00 00:00:00', 'admin'),
(85, 1, 3, '0.4', '2022-06-25 04:19:02', '0000-00-00 00:00:00', 'admin'),
(86, 1, 4, '0.5', '2022-06-25 04:19:25', '0000-00-00 00:00:00', 'admin'),
(87, 1, 5, '0.4', '2022-06-25 04:19:43', '0000-00-00 00:00:00', 'admin'),
(88, 1, 10, '0.5', '2022-06-25 04:20:05', '0000-00-00 00:00:00', 'admin'),
(89, 2, 68, '0.6', '2022-06-25 04:45:24', '0000-00-00 00:00:00', 'admin'),
(90, 2, 7, '0.5', '2022-06-25 04:45:41', '0000-00-00 00:00:00', 'admin'),
(91, 2, 8, '0.5', '2022-06-25 04:46:00', '0000-00-00 00:00:00', 'admin'),
(92, 2, 9, '0.7', '2022-06-25 04:46:17', '0000-00-00 00:00:00', 'admin'),
(93, 3, 3, '0.7', '2022-06-25 04:46:39', '0000-00-00 00:00:00', 'admin'),
(94, 3, 10, '0.6', '2022-06-25 04:46:58', '0000-00-00 00:00:00', 'admin'),
(95, 3, 11, '0.5', '2022-06-25 04:47:17', '0000-00-00 00:00:00', 'admin'),
(96, 3, 13, '0.8', '2022-06-25 04:47:45', '0000-00-00 00:00:00', 'admin'),
(97, 3, 12, '0.5', '2022-06-25 04:48:01', '0000-00-00 00:00:00', 'admin'),
(98, 4, 8, '0.7', '2022-06-25 04:48:17', '0000-00-00 00:00:00', 'admin'),
(99, 4, 15, '0.5', '2022-06-25 04:48:37', '0000-00-00 00:00:00', 'admin'),
(100, 4, 16, '0.5', '2022-06-25 04:48:55', '0000-00-00 00:00:00', 'admin'),
(101, 4, 17, '0.4', '2022-06-25 04:49:10', '0000-00-00 00:00:00', 'admin'),
(102, 5, 6, '0.5', '2022-06-25 04:49:55', '0000-00-00 00:00:00', 'admin'),
(103, 5, 10, '0.7', '2022-06-25 04:50:12', '0000-00-00 00:00:00', 'admin'),
(104, 5, 8, '0.4', '2022-06-25 04:50:42', '0000-00-00 00:00:00', 'admin'),
(105, 5, 3, '0.7', '2022-06-25 04:51:06', '0000-00-00 00:00:00', 'admin'),
(106, 6, 1, '0.6', '2022-06-25 04:51:29', '0000-00-00 00:00:00', 'admin'),
(107, 6, 20, '0.3', '2022-06-25 04:51:51', '0000-00-00 00:00:00', 'admin'),
(108, 6, 18, '0.6', '2022-06-25 04:52:08', '0000-00-00 00:00:00', 'admin'),
(109, 6, 19, '0.4', '2022-06-25 04:52:25', '0000-00-00 00:00:00', 'admin'),
(110, 7, 21, '0.7', '2022-06-25 04:52:41', '0000-00-00 00:00:00', 'admin'),
(111, 7, 22, '0.5', '2022-06-25 04:52:58', '0000-00-00 00:00:00', 'admin'),
(112, 7, 5, '0.4', '2022-06-25 04:53:14', '0000-00-00 00:00:00', 'admin'),
(113, 7, 19, '0.4', '2022-06-25 04:53:29', '0000-00-00 00:00:00', 'admin'),
(114, 7, 23, '0.6', '2022-06-25 04:53:45', '0000-00-00 00:00:00', 'admin'),
(115, 7, 24, '0.5', '2022-06-25 04:53:58', '0000-00-00 00:00:00', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gejala`
--

CREATE TABLE `gejala` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tgl_input` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `user_update_by` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `gejala`
--

INSERT INTO `gejala` (`id`, `kode`, `nama`, `tgl_input`, `tgl_update`, `user_update_by`) VALUES
(1, 'G01', 'Bau mulut', '2022-06-24 12:03:33', '2022-08-10 04:56:40', 'admin'),
(2, 'G02', 'Gusi bengkak, merah dan berdarah', '2022-06-24 12:03:57', '2022-08-10 04:56:51', 'admin'),
(3, 'G03', 'Gingiva berkaratin, gaung luka diantara gigi dan gusi', '2022-06-24 12:04:13', '2022-08-10 04:57:08', 'admin'),
(4, 'G04', 'Pembesaran limfoid di kepala, leher atau rahang', '2022-06-24 12:04:52', '2022-08-10 04:57:18', 'admin'),
(5, 'G05', 'Demam', '2022-06-24 12:05:19', '2022-08-10 04:57:27', 'admin'),
(6, 'G06', 'Nyeri gusi', '2022-06-24 12:05:33', '2022-08-10 04:57:36', 'admin'),
(7, 'G07', 'Sakit gigi', '2022-06-24 12:06:09', '2022-08-10 04:57:44', 'admin'),
(8, 'G08', 'Nyeri ringan hingga tajam saat mengonsumsi makanan manis', '2022-06-24 12:19:08', '2022-08-10 04:58:07', 'admin'),
(9, 'G09', 'Noda berwarna coklat, hitam atau putih pada permukaan gigi', '2022-06-24 12:19:23', '2022-08-10 04:58:19', 'admin'),
(10, 'G10', 'Nyeri saat menggigit makanan', '2022-06-24 12:20:18', '2022-08-10 04:58:28', 'admin'),
(11, 'G11', 'Gusi berdarah dan kemerahan', '2022-06-24 12:20:33', '2022-08-10 04:58:36', 'admin'),
(12, 'G12', 'Gusi membengkak dan atau bernanah', '2022-06-24 12:20:47', '2022-08-10 04:58:44', 'admin'),
(13, 'G13', 'Gusi melorot atau gigi tampak menjadi panjang', '2022-06-24 12:22:02', '2022-08-10 04:58:55', 'admin'),
(15, 'G15', 'Gigi menjadi regang (timbul celah diantara gigi)', '2022-06-24 12:23:28', '2022-08-10 04:59:35', 'admin'),
(16, 'G16', 'Gigi menjadi linu padahal tidak ada gigi yang berlubang', '2022-06-24 12:24:33', '2022-08-10 04:59:48', 'admin'),
(17, 'G17', 'Hilangnya nafsu makan', '2022-06-24 12:25:14', '2022-08-10 04:59:56', 'admin'),
(18, 'G18', 'Terdapat luka yang cukup besar di mulut', '2022-06-24 12:26:07', '2022-08-10 05:00:03', 'admin'),
(19, 'G19', 'Luka biasanya terjadi beberapa kali pada area yang sama', '2022-06-24 12:26:47', '2022-08-10 05:00:13', 'admin'),
(20, 'G20', 'Luka menyebar ke bagian luar bibir', '2022-06-24 12:27:05', '2022-08-10 05:00:22', 'admin'),
(21, 'G21', 'Tidak dapat makan dan minum', '2022-06-24 12:27:23', '2022-08-10 05:00:28', 'admin'),
(22, 'G22', 'Rasa pahit dimulut', '2022-06-24 12:27:38', '2022-08-10 05:00:39', 'admin'),
(23, 'G23', 'Gelisah', '2022-06-24 12:28:00', '2022-08-10 05:00:45', 'admin'),
(24, 'G24', 'Kelelahan', '2022-06-24 12:28:13', '2022-08-10 05:00:51', 'admin'),
(68, 'G25', 'Gusi mudah berdarah', '2022-06-25 04:44:58', '2022-08-10 05:00:58', 'admin'),
(69, 'G14', 'Gigi goyang dan sensitive', '2022-08-10 04:59:19', '0000-00-00 00:00:00', 'admin'),
(70, 'G26', 'Terdapat kantung nanah yang seperti benjolan dengan warna kuning', '2022-08-10 05:01:08', '0000-00-00 00:00:00', 'admin'),
(71, 'G27', 'Kelenjer getah bening di bawah rahang membengkak', '2022-08-10 05:01:16', '0000-00-00 00:00:00', 'admin'),
(72, 'G28', 'Mengunyah dan menelan makanan menyebabkan rasa nyeri', '2022-08-10 05:01:23', '0000-00-00 00:00:00', 'admin'),
(73, 'G29', 'Pecah-pecah dan kemerahan pada sudut mulut', '2022-08-10 05:01:31', '0000-00-00 00:00:00', 'admin'),
(74, 'G30', 'Muncul bintik-bintik kuning, putih atau krem di dalam mulut', '2022-08-10 05:01:39', '0000-00-00 00:00:00', 'admin'),
(75, 'G31', 'Sedikit pendarahan apabila lesi tergores', '2022-08-10 05:01:49', '0000-00-00 00:00:00', 'admin'),
(76, 'G32', 'Lesi menyerupai keju', '2022-08-10 05:02:00', '0000-00-00 00:00:00', 'admin'),
(77, 'G33', 'Di dalam mulut seperti kapas', '2022-08-10 05:02:20', '0000-00-00 00:00:00', 'admin'),
(78, 'G34', 'Kehilangan selera makan', '2022-08-10 05:02:29', '0000-00-00 00:00:00', 'admin'),
(79, 'G35', 'Mengunyah dan menelan makanan menyebabkan rasa nyeri', '2022-08-10 05:02:36', '0000-00-00 00:00:00', 'admin'),
(80, 'G36', 'Ujung-ujung gusi yang terletak diantara dua gigi mengalami pengikisan', '2022-08-10 05:02:44', '0000-00-00 00:00:00', 'admin'),
(81, 'G37', 'Rasa sakit dan tidak nyaman pada mulut saat makan makanan manis atau asam', '2022-08-10 05:02:51', '0000-00-00 00:00:00', 'admin'),
(82, 'G38', 'Rasa tidak nyaman pada saat cuaca dingin', '2022-08-10 05:02:59', '0000-00-00 00:00:00', 'admin'),
(83, 'G39', 'Sakit pada saat menyikat gigi', '2022-08-10 05:03:05', '0000-00-00 00:00:00', 'admin'),
(84, 'G40', 'Gusi menurun', '2022-08-10 05:03:12', '0000-00-00 00:00:00', 'admin'),
(85, 'G41', 'Peradangan pada lidah', '2022-08-10 05:03:19', '0000-00-00 00:00:00', 'admin'),
(86, 'G42', 'Permukaan lidah halus', '2022-08-10 05:03:26', '0000-00-00 00:00:00', 'admin'),
(87, 'G43', 'Lidah berwarna merah keputihan', '2022-08-10 05:03:33', '0000-00-00 00:00:00', 'admin'),
(88, 'G44', 'Alergi pada pasta gigi dan obat kumur', '2022-08-10 05:03:40', '0000-00-00 00:00:00', 'admin'),
(89, 'G45', 'Gusi berwarna merah terang', '2022-08-10 05:03:48', '0000-00-00 00:00:00', 'admin'),
(90, 'G46', 'Banyak luka terbuka berwarna putih dan kuning', '2022-08-10 05:03:59', '0000-00-00 00:00:00', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_grup_user`
--

CREATE TABLE `master_grup_user` (
  `id` int(11) NOT NULL,
  `nama_grup` varchar(30) NOT NULL,
  `tgl_input` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `user_update_by` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `master_grup_user`
--

INSERT INTO `master_grup_user` (`id`, `nama_grup`, `tgl_input`, `tgl_update`, `user_update_by`) VALUES
(1, 'Administrator', '2021-10-14 09:51:31', '2021-10-14 09:51:31', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_user`
--

CREATE TABLE `master_user` (
  `id` int(11) NOT NULL,
  `id_grup_user` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `userpass` text NOT NULL,
  `status_login` int(11) NOT NULL DEFAULT 0 COMMENT '0: blm login/sudah logout\r\n1: login',
  `usertoken` text DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `tgl_input` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `user_update_by` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `master_user`
--

INSERT INTO `master_user` (`id`, `id_grup_user`, `username`, `userpass`, `status_login`, `usertoken`, `last_login`, `last_logout`, `tgl_input`, `tgl_update`, `user_update_by`) VALUES
(1, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 0, NULL, NULL, '2022-08-10 05:38:32', '2022-06-19 10:07:00', '2022-06-19 05:42:43', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penanganan`
--

CREATE TABLE `penanganan` (
  `id` int(11) NOT NULL,
  `id_penyakit` int(11) NOT NULL,
  `penanganan` text NOT NULL,
  `tgl_input` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `user_update_by` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `penanganan`
--

INSERT INTO `penanganan` (`id`, `id_penyakit`, `penanganan`, `tgl_input`, `tgl_update`, `user_update_by`) VALUES
(1, 1, 'Melakukan scalling atau pembersihan karang gigi. Penambalan atau mengganti gigi yang rusak', '2022-08-10 05:14:23', '0000-00-00 00:00:00', 'admin'),
(2, 2, 'Melakukan floridasi, fisure sealent, gosok gigi 2 kali sehari serta rutin memeriksakan gigi dan melakukan pembersihan karang gigi di dokter setiap 6 bulan sekali', '2022-08-10 05:14:32', '0000-00-00 00:00:00', 'admin'),
(3, 3, 'Pembersihan karang gigi atau scalling', '2022-08-10 05:15:47', '0000-00-00 00:00:00', 'admin'),
(4, 4, 'Umumnya stomatitis mrmbsik dengan sendirinya. Asupan susu/jus', '2022-08-10 05:16:05', '0000-00-00 00:00:00', 'admin'),
(5, 5, 'Dianjurkan untuk segera memeriksakan diri ke dokter saat gejala muncul agar abses gigi tidak makin parah. Pemeriksaan kesehatan gigi dan mulut perlu dilakukan secara rutin ke dokter gigi. Hal ini bertujuan untuk menjaga kesehatan rongga mulut , serta mencegah atau mendeteksi lebih dini bila muncul penyakit. Pemeriksaan ke dokter gigi disarankan tiap 6 bulan sekali.', '2022-08-10 05:17:15', '0000-00-00 00:00:00', 'admin'),
(6, 6, 'Pembersihan jamur pada daerah yang terserang, pemberian obat anti jamur dan peningkatan DHE', '2022-08-10 05:17:23', '0000-00-00 00:00:00', 'admin'),
(7, 7, 'Pemberian antibiotik dan upaya untuk menjaga kebersihan mulut. Pada kasus yang berat tindakan pembedahan di butuhkan karena kerusakannya cukup luas', '2022-08-10 05:17:32', '0000-00-00 00:00:00', 'admin'),
(8, 15, 'Menggunakan pasa gigi khusus untuk gigi sensitif, memilih bulu sikat yang lembut, menggunkan obat kumur yang bebas alkohol, menyikat gigi secara perlahan. Segera lakukan pemeriksaan ke dokter gigi keluhan yang diraskan mengganggu kegiatan sehari-hari, seperti kesulitan mengunyah makanan.', '2022-08-10 05:17:38', '0000-00-00 00:00:00', 'admin'),
(9, 16, 'Pemberian antibiotik dan anti jamur. Mejaga kebersihan gigi dan mulut, misalnya gosok gigi minimal dua kali sehari serta memakai benang gigi (dental floss) untuk membersihkan sela-sela gigi.', '2022-08-10 05:17:46', '0000-00-00 00:00:00', 'admin'),
(10, 17, 'Pemberian obat antibiotik dan membersihkan area infeksi untuk memepercepat penyembuhan. Jika mulut terasa sakit, anda bisa mengonsumsi obat pereda nyeri seperti pacetamol atau ibuprofen. Konsumsi makanan sehat. Untuk sementara waktu, hindari makanan yang terlalu pedas, asin, atau asam. Makanan tersebut dapat memperparah atau mengiritasi luka pada mulut. Selalu menjaga kebersihan mulut dan gigi dengan sikat gigi dan flossing setiap hari.', '2022-08-10 05:17:55', '0000-00-00 00:00:00', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penyakit`
--

CREATE TABLE `penyakit` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `definisi` text NOT NULL,
  `tgl_input` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `user_update_by` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `penyakit`
--

INSERT INTO `penyakit` (`id`, `kode`, `nama`, `definisi`, `tgl_input`, `tgl_update`, `user_update_by`) VALUES
(1, 'P01', 'Gingivitis', 'Radang Gusi', '2022-06-24 11:55:07', '2022-08-10 04:54:14', 'admin'),
(2, 'P02', 'Karies Gigi', 'Gigi berlubang', '2022-06-24 11:55:37', '2022-08-10 04:54:31', 'admin'),
(3, 'P03', 'Kalkulius', 'Karang gigi', '2022-06-24 11:55:58', '2022-08-10 05:15:56', 'admin'),
(4, 'P04', 'Stomatitis', 'Sariawan', '2022-06-24 11:56:19', '2022-08-10 04:54:57', 'admin'),
(5, 'P05', 'Abses Periodontal', 'Infeksi pada jaringan penyangga', '2022-06-24 11:56:39', '2022-08-10 04:55:12', 'admin'),
(6, 'P06', 'Kandidiasi Oral', 'Jamur mulut', '2022-06-24 11:56:57', '2022-08-10 04:55:27', 'admin'),
(7, 'P07', 'Trench Mouth', 'Infeksi gusi', '2022-06-24 11:57:15', '2022-08-10 04:55:39', 'admin'),
(15, 'P08', 'Hypersensitivitas', 'Ngilu dan nyeri pada gigi', '2022-08-10 04:55:55', '0000-00-00 00:00:00', 'admin'),
(16, 'P09', 'Glositis', 'Perdangan atau infeksi pada lidah', '2022-08-10 04:56:07', '0000-00-00 00:00:00', 'admin'),
(17, 'P10', 'Gingivostomatitis', 'Infeksi rongga mulut karena virus', '2022-08-10 04:56:18', '0000-00-00 00:00:00', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `riwayat`
--

CREATE TABLE `riwayat` (
  `id` int(11) NOT NULL,
  `persen` int(10) NOT NULL,
  `id_alergi` int(11) NOT NULL,
  `tanggal` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `riwayat`
--

INSERT INTO `riwayat` (`id`, `persen`, `id_alergi`, `tanggal`) VALUES
(11, 88, 1, '2022-06-22'),
(12, 88, 1, '2022-06-22'),
(13, 88, 1, '2022-06-22'),
(14, 30, 2, '2022-06-22'),
(15, 89, 1, '2022-06-22');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `diagnosa`
--
ALTER TABLE `diagnosa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `gejala`
--
ALTER TABLE `gejala`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `master_grup_user`
--
ALTER TABLE `master_grup_user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `master_user`
--
ALTER TABLE `master_user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `penanganan`
--
ALTER TABLE `penanganan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `penyakit`
--
ALTER TABLE `penyakit`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `riwayat`
--
ALTER TABLE `riwayat`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `diagnosa`
--
ALTER TABLE `diagnosa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT untuk tabel `gejala`
--
ALTER TABLE `gejala`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT untuk tabel `master_grup_user`
--
ALTER TABLE `master_grup_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `master_user`
--
ALTER TABLE `master_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `penanganan`
--
ALTER TABLE `penanganan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `penyakit`
--
ALTER TABLE `penyakit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `riwayat`
--
ALTER TABLE `riwayat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
